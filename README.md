# Curso: Database Programming with SQL

[1. Crear tablas (create table - describe - all_tables - drop table)](manual/1.md)

[2. Ingresar registros (insert into- select)](manual/2.md)

[3. Tipos de datos](manual/3.md)

[4. Recuperar algunos campos (select)](manual/4.md)

[5. Recuperar algunos registros (where)](manual/5.md)

[6. Operadores relacionales](manual/6.md)

[7. Borrar registros (delete)](manual/7.md)

[8. Actualizar registros (update)](manual/8.md)

[9. Comentarios](manual/9.md)

[10. Valores nulos (null)](manual/10.md)

[11. Operadores relacionales (is null)](manual/11.md)

[12. Clave primaria (primary key)](manual/12.md)

[13. Vaciar la tabla (truncate table)](manual/13.md)

[14. Tipos de datos alfanuméricos](manual/14.md)

[15. Tipos de datos numéricos](manual/15.md)

[16. Ingresar algunos campos](manual/16.md)

[17. Valores por defecto (default)](manual/17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](manual/18.md)

[19.  Alias (encabezados de columnas)](manual/19.md)

[20. Funciones string](manual/20.md)

[21. Funciones matemáticas](manual/21.md)

[22. Funciones de fechas y horas](manual/22.md)

[23. Ordenar registros (order by)](manual/23.md)

[24. Operadores lógicos (and - or - not)](manual/24.md)

[25. Otros operadores relacionales (between)](manual/25.md)

[26. Otros operadores relacionales (in)](manual/26.md)

[27. Búsqueda de patrones (like - not like)](manual/27.md)

[28. Contar registros (count)](manual/28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](manual/28.md)

[30. Agrupar registros (group by)](manual/30.md)

[31. Seleccionar grupos (Having)](manual/31.md)

[32. Registros duplicados (Distinct)](manual/32.md)

[33. Clave primaria compuesta](manual/33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](manual/34.md)

[35. Alterar secuencia (alter sequence)](manual/35.md)

[36. Integridad de datos](manual/36.md)

[37. Restricción primary key](manual/37.md)

[38. Restricción unique](manual/38.md)

[39. Restriccioncheck](manual/39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](manual/40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](manual/41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](manual/42.md)

[43. Indices](manual/43.md)

[44. Indices (Crear . Información)](manual/44.md)

[45. Indices (eliminar)](manual/45.md)

[46. Varias tablas (join)](manual/46.md)

[47. Combinación interna (join)](manual/47.md)

[48. Combinación externa izquierda (left join)](manual/48.md)

[49. Combinación externa derecha (right join)](manual/49.md)

[50. Combinación externa completa (full join)](manual/50.md)

[51. Combinaciones cruzadas (cross)](manual/51.md)

[52. Autocombinación](manual/52.md)

[53. Combinaciones y funciones de agrupamiento](manual/53.md)

[54. Combinar más de 2 tablas](manual/54.md)

[55. Otros tipos de combinaciones](manual/55.md)

[56. Clave foránea](manual/56.md)

[57. Restricciones (foreign key)](manual/57.md)

[58. Restricciones foreign key en la misma tabla](manual/58.md)

[59. Restricciones foreign key (eliminación)](manual/59.md)

[60. Restricciones foreign key deshabilitar y validar](manual/60.md)

[61. Restricciones foreign key (acciones)](manual/61.md)

[62. Información de user_constraints](manual/62.md)

[63. Restricciones al crear la tabla](manual/63.md)

[64. Unión](manual/64.md)

[65. Intersección](manual/65.md)

[66. Minus](manual/66.md)

[67. Agregar campos (alter table-add)](manual/67.md)

[68. Modificar campos (alter table - modify)](manual/68.md)

[69. Eliminar campos (alter table - drop)](manual/69.md)

[70.  Agregar campos y restricciones (alter table)](manual/70.md)

[71. Subconsultas](manual/71.md)

[72. Subconsultas como expresion](manual/72.md)

[73. Subconsultas con in](manual/73.md)

[74. Subconsultas any- some - all](manual/74.md)

[75. Subconsultas correlacionadas](manual/75.md)

[76. Exists y No Exists](manual/76.md)

[77. Subconsulta simil autocombinacion](manual/77.md)

[78. Subconsulta conupdate y delete](manual/78.md)

[79. Subconsulta e insert](manual/79.md)

[80. Crear tabla a partir de otra (create table-select)](manual/80.md)

[81. Vistas (create view)](manual/81.md)

[82. Vistas (información)](manual/82.md)

[83. Vistas eliminar (drop view)](manual/83.md)

[84. Vistas (modificar datos a través de ella)](manual/84.md)

[85. Vistas (with read only)](manual/85.md)

[86. Vistas modificar (create or replace view)](manual/86.md)

[87. Vistas (with check option)](manual/87.md)

[88. Vistas (otras consideraciones: force)](manual/88.md)

[89. Vistas materializadas (materialized view)](manual/89.md)

[90. Procedimientos almacenados](manual/90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](manual/91.md)

[92. Procedimientos Almacenados (eliminar)](manual/92.md)

[93. Procedimientos almacenados (parámetros de entrada)](manual/93.md)

[94. Procedimientos almacenados (variables)](manual/94.md)

[95. Procedimientos Almacenados (informacion)](manual/95.md)

[96. Funciones](manual/96.md)

[97. Control de flujo (if)](manual/97.md)

[98. Control de flujo (case)](manual/98.md)

[99. Control de flujo (loop)](manual/99.md)

[100. Control de flujo (for)](manual/100.md)

[101. Control de flujo (while loop)](manual/101.md)

[102. Disparador (trigger)](manual/102.md)

[103. Disparador (información)](manual/103.md)

[104. Disparador de inserción a nivel de sentencia](manual/104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](manual/105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](manual/106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](manual/107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](manual/108.md)

[109. Disparador de actualización - lista de campos (update trigger)](manual/109.md)

[110. Disparador de múltiples eventos](manual/110.md)

[111. Disparador (old y new)](manual/111.md)

[112. Disparador condiciones (when)](manual/112.md)

[113. Disparador de actualizacion - campos (updating)](manual/113.md)

[114. Disparadores (habilitar y deshabilitar)](manual/114.md)

[115. Disparador (eliminar)](manual/115.md)

[116. Errores definidos por el usuario en trigger](manual/116.md)

[117. Seguridad y acceso a Oracle](manual/117.md)

[118. Usuarios (crear)](manual/118.md)

[119. 	Permiso de conexión](manual/119.md)

[120. Privilegios del sistema (conceder)](manual/120.md)

[121. Privilegios del sistema (with admin option)](manual/121.md)

[122. Modelado de base de datos](manual/122.md)

## Modelo de presentacion

[repositorio](https://gitlab.com/senati7912512/base-datos)

## Libros referenciales

[Oracle 12c FORMS y REPORTS-Curso práctico de formación](https://drive.google.com/file/d/1vlQioFvdtYCoPY6jY0Tq_UTaro3wMjcm/edit)

[Sistemas Gestores de Bases de Datos](https://drive.google.com/file/d/1VYaSpy-uT1lghwK9hPjAr6U4BBsbBQ9D/edit)

[Administración básica de base de datos con Oracle 12c SQL](https://drive.google.com/file/d/1HGcdUc_SgbaovtvEP1uBV9ZTUbSq_lPJ/edit)

## Backup's

[HR Data Base](https://drive.google.com/file/d/1uOe4LY18W32i5yWcazwf1k-NNXMmTIXs/edit)

## Presentaciones del curso de Oracle Academy

## 1. Introducción

[1.1. Oracle Application Express](https://drive.google.com/file/d/1hjve3XEb0mpr6KzFhkFet4muM4upDkpR/edit)

[1.2. Tecnología de Base de Datos Relacional](https://drive.google.com/file/d/1Mwc5rv6ZZ96SYQaf4ixqeeaFjPrYlfsL/edit)

[1.3. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

[1.4. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

## Presentaciones adicionales

[1. Introducción](https://docs.google.com/presentation/d/19Fq-uNNwYiuTIOtJxIlBD7a9uOQrmwv9/edit)

## Repositorio del curso

[Gitlab](https://gitlab.com/senati7912512/base_datos_oracle)

## Modelado de base de datos

[Manual](modelado/README.md)

[GlobalHome](GlobalHome/README.md)

## Practicas modelado

[Laboratorio practico ORACLE](practicas/Laboratorio_practico_ORACLE.pdf)


## Practica de habilidades en SQL

[Desarrollar los siguientes ejercicios](practicas/Ejercicios.pdf)