# 25. Otros operadores relacionales (between)

Hemos visto los operadores relacionales: = (igual), <> (distinto), > (mayor), < (menor), >= (mayor o igual), <= (menor o igual), is null/is not null (si un valor es NULL o no).

Otro operador relacional es "between", trabajan con intervalos de valores.

Hasta ahora, para recuperar de la tabla "libros" los libros con precio mayor o igual a 20 y menor o igual a 40, usamos 2 condiciones unidas por el operador lógico "and":

```sql
select * from libros
where precio>=20 and precio<=40;
```

Podemos usar "between" y así simplificar la consulta:

```sql
select * from libros
where precio between 20 and 40;
```

Averiguamos si el valor de un campo dado (precio) está entre los valores mínimo y máximo especificados (20 y 40 respectivamente).

"between" significa "entre". Trabaja con intervalo de valores.

Este operador no tiene en cuenta los valores "null".

Si agregamos el operador "not" antes de "between" el resultado se invierte, es decir, se recuperan los registros que están fuera del intervalo especificado. Por ejemplo, recuperamos los libros cuyo precio NO se encuentre entre 20 y 30, es decir, los menores a 20 y mayores a 30:

```sql
select * from libros
where precio not between 20 and 30;
```

Podemos especificar un intervalo de valores de tipo fecha con "between":

```sql
select * from libros
where edicion between '01/05/2000' and '01/05/2007';
```

Entonces, empleamos el operador "between" para reducir las condiciones "where".

## Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla y la creamos con la siguiente estructura:

```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values(1,'El aleph','Borges','Emece','15/01/2000',15.90);

insert into libros
values(2,'Cervantes y el quijote','Borges','Paidos',null,null);

insert into libros
values(3,'Alicia en el pais de las maravillas','Lewis Carroll',null,'25/03/2000',19.90);

insert into libros
values(4,'Martin Fierro','Jose Hernandez','Emece','18/05/2000',25.90);

insert into libros (codigo,titulo,autor,edicion,precio)
values(5,'Antología poética','Borges','25/08/2000',32);

insert into libros (codigo,titulo,autor,edicion,precio)
values(6,'Java en 10 minutos','Mario Molina','11/02/2007',45.80);

insert into libros (codigo,titulo,autor,edicion,precio)
values(7,'Martin Fierro','Jose Hernandez','23/11/2006',40);

insert into libros (codigo,titulo,autor,edicion,precio)
values(8,'Aprenda PHP','Mario Molina','01/06/2007',56.50);
```

Recuperamos los registros cuyo precio esté entre 20 y 40 empleando "between":

```sql
select * from libros
where precio between 20 and 40;
```

Note que si el campo tiene el valor "null", no aparece en la selección.

Para seleccionar los libros cuyo precio NO esté entre un intervalo de valores antecedemos "not" al "between":

```sql
select * from libros
where precio not between 20 and 40;
```

Note que si el campo tiene el valor "null", no aparece en la selección.

Recuperamos los títulos y edición de los libros cuya fecha de edición se encuentre entre '01/05/2000' y '01/05/2007', ordenados por fecha de edición:

```sql
select titulo, edicion from libros
where edicion between '01/05/2000' and '01/05/2007'
order by edicion;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:
 
```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

insert into libros
values(1,'El aleph','Borges','Emece','15/01/2000',15.90);

insert into libros
values(2,'Cervantes y el quijote','Borges','Paidos',null,null);

insert into libros
values(3,'Alicia en el pais de las maravillas','Lewis Carroll',null,'25/03/2000',19.90);

insert into libros
values(4,'Martin Fierro','Jose Hernandez','Emece','18/05/2000',25.90);

insert into libros (codigo,titulo,autor,edicion,precio)
values(5,'Antología poética','Borges','25/08/2000',32);

insert into libros (codigo,titulo,autor,edicion,precio)
values(6,'Java en 10 minutos','Mario Molina','11/02/2007',45.80);

insert into libros (codigo,titulo,autor,edicion,precio)
values(7,'Martin Fierro','Jose Hernandez','23/11/2006',40);

insert into libros (codigo,titulo,autor,edicion,precio)
values(8,'Aprenda PHP','Mario Molina','01/06/2007',56.50);

select * from libros
where precio between 20 and 40;

select * from libros
where precio not between 20 and 40;

select titulo, edicion from libros
where edicion between '01/05/2000' and '01/05/2007'
order by edicion;
```

## Ejercicios propuestos

## Ejericio 01

En una página web se guardan los siguientes datos de las visitas: nombre, mail, país y fecha de la visita.

1. Elimine la tabla "visitas" y créela con la siguiente estructura:

```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);
```

2. Ingrese algunos registros:

```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina','10/10/2016');

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@gotmail.com>','Chile','10/10/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','11/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@gmail.com>','Argentina','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','15/09/2016');

insert into visitas
values ('Federico1','<federicogarcia@xaxamail.com>','Argentina',null);
```

3. Seleccione los usuarios que visitaron la página entre el '12/09/2016' y '11/10/2016' (6 registros)
Note que incluye los de fecha mayor o igual al valor mínimo y menores o iguales al valor máximo, y que los valores nulos no se incluyen.

## Ejercicio 02

Trabaje con la tabla llamada "medicamentos" de una farmacia.

1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table medicamentos;

create table medicamentos(
    codigo number(6) not null,
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(4),
    fechavencimiento date not null,
    primary key(codigo)
);
```

2. Ingrese algunos registros:

```sql
insert into medicamentos
values(102,'Sertal','Roche',5.2,10,'01/02/2020');

insert into medicamentos
values(120,'Buscapina','Roche',4.10,200,'01/12/2017');

insert into medicamentos
values(230,'Amoxidal 500','Bayer',15.60,100,'28/12/2017');

insert into medicamentos
values(250,'Paracetamol 500','Bago',1.90,20,'01/02/2018');

insert into medicamentos
values(350,'Bayaspirina','Bayer',2.10,150,'01/12/2019');

insert into medicamentos
values(456,'Amoxidal jarabe','Bayer',5.10,250,'01/10/2020');
```

3. Recupere los nombres y precios de los medicamentos cuyo precio esté entre 5 y 15 (2 registros)

4. Seleccione los registros cuya cantidad se encuentre entre 100 y 200 (3 registros)

5. Recupere los remedios cuyo vencimiento se encuentre entre la fecha actual y '01/01/2028' inclusive.

6. Elimine los remedios cuyo vencimiento se encuentre entre el año 2017 y 2018 inclusive (3 registros)
