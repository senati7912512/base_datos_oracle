# 112. Disparador condiciones (when)

En los triggers a nivel de fila, se puede incluir una restricción adicional, agregando la clausula "when" con una condición que se evalúa para cada fila que afecte el disparador; si resulta cierta, se ejecutan las sentencias del trigger para ese registro; si resulta falsa, el trigger no se dispara para ese registro.

Limitaciones de "when":

- no puede contener subconsultas, funciones agregadas ni funciones definidas por el usuario;

- sólo se puede hacer referencia a los parámetros del evento;

- no se puede especificar en los trigers "instead of" ni en trigger a nivel de sentencia.

Creamos el siguiente disparador:

```sql
create or replace trigger tr_precio_libros
before insert orupdate of precio
on libros
    for each row when(new.precio>50)
begin
    :new.precio := round(:new.precio);
end tr_precio_libros;
/
```

El disparador anterior se dispara ANTES (before) que se ejecute un "insert" sobre "libros" o un "update" sobre "precio" de "libros". Se ejecuta una vez por cada fila afectada (for each row) y solamente si cumple con la condición del "when", es decir, si el nuevo precio que se ingresa o modifica es superior a 50. Si el precio es menor o igual a 50, el trigger no se dispara. Si el precio es mayor a 50, se modifica el valor ingresado redondeándolo a entero.

Note que cuando hacemos referencia a "new" (igualmente con "old") en la condición "when", no se colocan los dos puntos precediéndolo; pero en el cuerpo del trigger si.

Si ingresamos un registro con el valor 30.80 para "precio", el trigger no se dispara.

Si ingresamos un registro con el valor "55.6" para "precio", el trigger se dispara modificando tal valor a "56".

Si actualizamos el precio de un libro a "40.30", el trigger no se activa.

Si actualizamos el precio de un libro a "50.30", el trigger se activa y modifica el valor a "50".

Si actualizamos el precio de 2 registros a valores que superen los "50", el trigger se activa 2 veces redondeando los valores a entero.

Si actualizamos en una sola sentencia el precio de 2 registros y solamente uno de ellos supera los "50", el trigger se activa 1 sola vez.

El trigger anterior podría haberse creado de la siguiente manera:

```sql
create or replace trigger tr_precio_libros
before insert orupdate of precio
on libros
    for each row
begin
    if :new.precio>50 then
        :new.precio := round(:new.precio);
    end if;
end tr_precio_libros;
/
```

En este caso, la condición se chequea en un "if" dentro del cuerpo del trigger. La diferencia con el primer trigger que contiene "when" es que la condición establecida en el "when" se testea antes que el trigger se dispare y si resulta verdadera, se dispara el trigger, sino no. En cambio, si la condición está dentro del cuerpo del disparador, el trigger se dispara y luego se controla el precio, si cumple la condición, se modifica el precio, sino no.

Por ejemplo, la siguiente sentencia:

```sql
update libros set precio=40 where...;
```

no dispara el primer trigger, ya que no cumple con la condición del "when"; pero si dispara el segundo trigger, que no realiza ninguna acción ya que al evaluarse la condición del "if", resulta falsa.

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros".

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla con la siguiente estructura:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Planeta',40);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
```

Creamos un trigger a nivel de fila que se dispara "antes" que se ejecute un "insert" o un "update" sobre el campo "precio" de la tabla "libros". Se activa solamente si el nuevo precio que se ingresa o se modifica es superior a 50, en caso de serlo, se modifica el valor ingresado redondeándolo a entero:

```sql
create or replace trigger tr_precio_libros
before insert orupdate of precio
on libros
    for each row when(new.precio>50)
begin
    :new.precio := round(:new.precio);
end tr_precio_libros;
/
```

Ingresamos un registro con el valor 30.80 para "precio":

```sql
insert into libros values(250,'El experto en laberintos','Gaskin','Emece',30.80);
```

El trigger no se dispara.

Veamos si el precio ingresado se redondeó:

```sql
select *from libros where titulo like '%experto%';
```

El precio no se redondeó porque no es superior a 50, el trigger no se disparó.

Ingresamos un registro con el valor "55.6" para "precio":

```sql
insert into libros values(300,'Alicia en el pais de las maravillas','Carroll','Emece',55.6);
```

Consultamos "libros":

```sql
select *from libros where titulo like '%maravillas%';
```

El trigger se disparó y se redondeó el nuevo precio a 56.

Actualizamos el precio de un libro a "40.30":

```sql
update libros set precio=40.30 where codigo=105;
```

Consultamos "libros":

```sql
select *from libros where codigo =105;
```

Se almacenó el valor tal como se solicitó, el trigger no se disparó ya que ":new.precio" no cumplió con la condición del "when".

Actualizamos el precio de un libro a "50.30":

```sql
update libros set precio=50.30 where codigo=105;
```

Consultamos la tabla:

```sql
select *from libros where codigo=105;
```

El trigger se activa porque ":new.precio" cumple con la condición "when" y modifica el valor a "50".

Actualizamos el precio de 2 registros a "50.30":

```sql
update libros set precio=50.30 where editorial='Nuevo siglo';
```

Consultamos:

```sql
select *from libros where editorial='Nuevo siglo';
```

El trigger se activa 2 veces redondeado el valor a 50.

Ejecutamos el siguiente "update":

```sql
update libros set precio=precio+15.8 where editorial='Planeta';
```

Consultamos:

```sql
select *from libros where editorial='Planeta';
```

De los dos libros de editorial "Planeta" solamente uno supera el valor 50, por lo tanto, el trigger se dispara una sola vez.

Activamos el paquete "dbms_output":

```sql
set serveroutput on;
execute dbms_output.enable(20000);
```

Reemplazamos el trigger anterior por uno sin condición "when". La condición se controla en un "if" en el interior del trigger. En este caso, el trigger se dispara SIEMPRE que se actualice un precio en "libros", dentro del trigger se controla el precio, si cumple la condición, se modifica, sino no:

```sql
create or replace trigger tr_precio_libros
before insert orupdate of precio
on libros
    for each row
begin
    dbms_output.put_line('Trigger disparado');
    if :new.precio>50 then
        dbms_output.put_line('Precio redondeado');
        :new.precio:= round(:new.precio);
    end if;
end tr_precio_libros;
/
```

Note que agregamos una salida de texto para controlar si el trigger se ha disparado y otra, para controlar si entra por la condición "if".

Ingresamos un registro con un valor inferior a 50 para "precio":

```sql
insert into libros values(350,'Ilusiones','Bach','Planeta',20.35);
```

El trigger se dispara (aparece el primer mensaje), pero no realiza ninguna acción ya que al evaluarse la condición del "if", resulta falsa.

Ingresamos un registro con un valor superior a 50 para "precio":

```sql
insert into libros values(380,'El anillo del hechicero','Gaskin','Planeta',60.35);
```

El trigger se dispara (aparece el primer mensaje) y al evaluarse como cierta la condición, realiza la acción (aparece el segundo mensaje).

Consultamos el diccionario para ver qué nos informa sobre el disparador recientemente creado:

```sql
select *from user_triggers where trigger_name ='TR_PRECIO_LIBROS';
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados".

1. Elimine la tabla:

```sql
drop table empleados;
```

2. Cree la tabla con la siguiente estructura:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);
```

3. Ingrese algunos registros:

```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);
```

4. La empresa necesita controlar cuando se le aumenta el sueldo a los empleados, guardando en una tabla denominada "control", el nombre del usuario, la fecha, el documento de quien se ha modificado el sueldo, el antiguo sueldo y el nuevo sueldo. Para ello cree la tabla control (antes elimínela por si existe):

```sql
drop table control;
create table control(
    usuario varchar2(30),
    fecha date,
    documento char(8),
    antiguosueldo number(8,2),
    nuevosueldo number(8,2)
);
```

5. Cree un disparador que almacene el nombre del usuario, la fecha, documento, el antiguo y el nuevo sueldo en "control" cada vez que se actualice un sueldo de la tabla "empleados" a un valor mayor. Si el sueldo se disminuye, el trigger no debe activarse. Si se modifica otro campo diferente de "sueldo", no debe activarse.

6. Actualice el sueldo de todos los empleados de la sección "Sistemas" a "1000"

7. Consulte la tabla "control" para ver cuántas veces se ha disparado el trigger
Se ha disparado una sola vez; se actualizaron 2 registros, pero en solo uno de ellos se aumentó el sueldo.

8. Al empleado con documento "22333444" se lo ha cambiado a la sección "contaduria". Realice el cambio en la tabla "empleados"

9. Verifique que el trigger no se ha activado porque no se ha modificado el campo "sueldo". Consulte "control"

10. Cree un disparador a nivel de fila que se dispare cada vez que se ingrese un nuevo empleado y coloque en mayúsculas el apellido ingresado. Además, si no se ingresa sueldo, debe ingresar '0'

11. Ingrese un nuevo empleado empleando minúsculas en el apellido

12. Verifique que el trigger "tr_ingresar_empleados" se disparó

13. Ingrese dos nuevos empleados, uno sin sueldo y otro con sueldo "null"

14. Verifique que el trigger "tr_ingresar_empleados" se ha disparado
Los dos registros deben tener el apellido en mayúsculas y deben tener el valor '0' en sueldo.

15. Cree un disparador a nivel de fila que se dispare cada vez que se ingresa un nuevo empleado y coloque "null" en "sueldo" si el sueldo ingresado supera los $1000 o es inferior a $500

16. Ingrese un nuevo empleado con un sueldo que dispare el trigger creado anteriormente

17. Verifique que el trigger "tr_ingresar_empleados" se disparó
